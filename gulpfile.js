var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

var paths = {
  src: ['./lib/shim.js', './src/interval.js'],
  dist: './dist/'
};

gulp.task('build', function() {
  gulp
   .src(paths.src)
   .pipe(uglify())
   .pipe(concat('interval.js'))
   .pipe(gulp.dest(paths.dist));
});

gulp.task('watch', function() {
  gulp.watch(paths.src, ['build']);
});

gulp.task('default', ['build', 'watch']);
