var express = require('express');
var interval = require('./../dist/interval');

var app = express();

app.get('/', function(req, res){
  interval.start(function() {
    console.log('interval firing');

  }, 1000);

  setTimeout(function() {
    interval.clear(function() {
      console.log('clearing interval');
    });
  }, 3000);

  res.send('hello world');
});

app.listen(3000);
