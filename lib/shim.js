// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating

// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel

// MIT license

(function(root) {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !root.requestAnimationFrame; ++x) {
        root.requestAnimationFrame = root[vendors[x]+'RequestAnimationFrame'];
        root.cancelAnimationFrame = root[vendors[x]+'CancelAnimationFrame'] ||
        root[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (typeof root.requestAnimationFrame === 'undefined')
        root.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = root.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (typeof root.cancelAnimationFrame === 'undefined')
        root.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}(typeof window === 'undefined' ? global : window));
