/**
 * Code for start function used from Joe Lambert's gist
 * https://gist.github.com/joelambert/1002116
 */

(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define([], function() {
            return (root.Interval = factory(root));
        });
    } else if (typeof exports === 'object') {
        module.exports = factory(root);
    } else {
        root.Interval = factory(root);
    }
}(typeof window === 'undefined' ? global : window, function (root) {

    var exports = {};

    exports.start = function(fn, timing) {
      var _this = this;
      var start = new Date().getTime();
      var delay = timing || 16; // 16 for ~60fps

      function loop() {
    		var current = new Date().getTime();
    		var delta = current - start;

    		if (delta >= delay) {
    			fn.call();
    			start = new Date().getTime();
    		}

    	  _this.interval = root.requestAnimationFrame(loop);
    	}

      _this.interval = root.requestAnimationFrame(loop);
    };

    exports.clear = function(fn) {
      cancelAnimationFrame(this.interval);

      fn.call();
    };

    return exports;
}));
